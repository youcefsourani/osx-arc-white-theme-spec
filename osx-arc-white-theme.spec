%global debug_package %{nil}
Name:           osx-arc-white-theme
Version:        1.1
Release:        1%{?dist}
Summary:        OSX Arc White Theme
Group:		User Interface/Desktops
License:        GPLv3
URL:            https://www.gnome-look.org/p/1013344/
Source0:        https://dl.opendesktop.org/api/files/download/id/1470839268/OSX-Arc-White-v-%{version}.tar.gz
BuildArch:	noarch
Requires:	filesystem

%description
OSX Arc White Theme



%prep
%setup -q -n OSX-Arc-White


%build



%install
rm -rf $RPM_BUILD_ROOT
%{__mkdir} -p %{buildroot}%{_datadir}/themes/%{name}
%{__cp} -rf * %{buildroot}%{_datadir}/themes/%{name}



%files
%{_datadir}/themes/%{name}



%changelog
* Mon Sep 19 2016 youcef sourani youssef.m.sourani@gmail.com - 1.1-1
- Initial for Fedora


